# README #

This is an authentication script for IIT(ISM) Employees and Students. It is mostly the same as the script available [here](https://github.com/swapnilsm/firewall-auth-py3).
For the options (e.g. to use a netrc file to store the credentials), see the details in the above repository. Special thanks to [Swapnil Mahajan](https://github.com/swapnilsm).

### Simple Usage ###

* Make sure your system has [Python3](https://www.python.org/download/releases/3.0/) installed.
* Download the authenticator script.
* Execute the command:**
```
python3 authenticator.py
```**
* Type your username and password (the password will not be shown on your screen when you type it).
* If everything goes as expected, you should see a message **INFO - Sending request to keep alive.**
* Minimize the terminal or Command Prompt with the script and you should not see the authentication page ever again !!

### Note: Sign-out of the ISM authentication page before running this script###
